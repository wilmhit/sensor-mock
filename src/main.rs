use clap::Parser;
use rand::{prelude::ThreadRng, Rng};
use std::fs;
use std::io::Write;
use std::thread::sleep;
use std::time::Duration;

const CHANGE_RATE: i8 = 20;

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Engine control file
    #[clap(short, long)]
    input: String,

    /// Sensor temperature file
    #[clap(short, long)]
    output: String,
}

fn main() {
    let cli = Args::parse();
    let input_path = &cli.input;
    let output_path = &cli.output;
    let mut rng = rand::thread_rng();
    loop {
        let engine_control = read_number(input_path);
        let current_value = read_number(output_path);
        let diff = generate_diff(engine_control, &mut rng);
        apply_diff(output_path, current_value, diff);

        println!("{}: {}", output_path, current_value);
        println!("{}: {}", input_path, engine_control);
        println!("======");

        sleep(Duration::from_secs(1));
    }
}

fn read_number(file_path: &str) -> u8 {
    let mut file_bytes = fs::read(file_path).unwrap();
    file_bytes = strip_non_decimal(&file_bytes);
    let input = String::from_utf8(file_bytes).unwrap();
    let number: u8 = input.parse().unwrap();
    number
}

fn generate_diff(throttle: u8, rng: &mut ThreadRng) -> i8 {
    let diff = rng.gen_range(0..CHANGE_RATE);
    if throttle == 0 {
        return -diff;
    } else {
        return diff;
    }
}

fn strip_non_decimal(buf: &Vec<u8>) -> Vec<u8> {
    buf.iter()
        .filter(|char| char.is_ascii_digit())
        .copied()
        .collect()
}

/// Returns value from file before applying diff
fn apply_diff(file_path: &str, current_value: u8, diff: i8) {
    let mut file = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(file_path)
        .unwrap();
    let mut number = current_value.to_owned();

    if diff.is_negative() {
        number = number.saturating_sub(diff.abs() as u8);
    } else {
        number = number.saturating_add(diff as u8);
    }

    let mut write_buf = number.to_string().as_bytes().to_owned();
    write_buf.append(&mut vec![10]); //If you wish to use LFCR do [10, 13]
    file.write_all(&write_buf).unwrap();
}
